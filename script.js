/* 
	=======================
	Scene
	=======================
	*/
var scene = new THREE.Scene();
scene.background = new THREE.Color(0xffffff);

var canvas = document.getElementById("canvas"),
  renderer = new THREE.WebGLRenderer({ canvas: canvas, alpha: false }),
  WIDTH = document.documentElement.clientWidth,
  HEIGHT = document.documentElement.clientHeight;

var shapes = [],
  population = { x: 50, z: 30 },
  simplex = new SimplexNoise(),
  iteration = 0,
  params = {
    simplexVariation: 0.023,
    simplexAmp: 10,
    opacity: 0.02
  };
//
// var gui;
// gui = new dat.GUI();
// gui
//   .add(params, "simplexVariation")
//   .min(0)
//   .max(0.2)
//   .step(0.0001)
//   .name("Frequency");
// gui.add(params, "simplexAmp").min(0).max(10).step(0.1).name("Amplitude");
// gui.add(params, "opacity").min(.01).max(.5).name("Alpha");
// gui.open();

renderer.setSize(WIDTH, HEIGHT);

/* 
	=======================
	Camera
	=======================
	*/
var camera = new THREE.PerspectiveCamera(65, WIDTH / HEIGHT, 0.1, 120);

camera.position.x = 0;
camera.position.y = 0;
camera.position.z = 37;

scene.add(camera);

/* 
	=======================
	Lights
	=======================
	*/
var ambiantlight = new THREE.AmbientLight(0x555555);
scene.add(ambiantlight);

var light = new THREE.DirectionalLight(0xffffff, 1);
light.position.set(0, 30, 0);
scene.add(light);

var light2 = new THREE.DirectionalLight(0xffffff, 1);
light2.position.set(30, 30, 0);
scene.add(light2);

/* 
	=======================
	Mesh
	=======================
	*/

function Cube(x, z) {
  this.speed = Math.floor(Math.random() * 100) / 100;
  this.geometry = new THREE.BoxGeometry(1, 1, 1);
  this.material = new THREE.MeshPhongMaterial({
    color: 0x000000,
    reflectivity: 0,
    transparent: true,
    opacity: params.opacity,
    shininess: 100,
    specular: 0x00ffff,
    shading: THREE.FlatShading
  });
  this.mesh = new THREE.Mesh(this.geometry, this.material);
  this.mesh.position.x = x;
  this.mesh.position.y = 0;
  this.mesh.position.z = z;
}

Cube.prototype.move = function() {
  this.material.opacity = params.opacity;
  this.mesh.position.y =
    simplex.noise4d(
      this.mesh.position.x * params.simplexVariation,
      this.mesh.position.y * params.simplexVariation,
      this.mesh.position.z * params.simplexVariation,
      iteration / 200
    ) * params.simplexAmp;
};

for (var i = population.x * -0.5; i <= population.x / 2; i++) {
  for (var u = population.z * -0.5; u <= population.z / 2; u++) {
    shapes[shapes.length] = new Cube(i, u);
    scene.add(shapes[shapes.length - 1].mesh);
  }
}

/* 
	=======================
	Logic
	=======================
	*/
camera.lookAt(scene);
renderer.render(scene, camera);

controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.enableDamping = true;
controls.dampingFactor = 0.25;
controls.enableZoom = true;

function animate() {
  iteration++;
  requestAnimationFrame(animate);

  for (var i in shapes) {
    shapes[i].move();
  }

  renderer.render(scene, camera);
}
animate();


window.addEventListener('resize', function() {
  WIDTH = window.innerWidth;
  HEIGHT = window.innerHeight;
  renderer.setSize(WIDTH,HEIGHT);
  camera.aspect = WIDTH / HEIGHT;
  camera.updateProjectionMatrix();
});