$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')




// INCLUDE FUNCTION

var equalheight 	= $(".equalheight"),
	equalheight2 	= $(".equalheight2"),
	owl             = $(".owl-carousel"),
	// popup           = $("[data-popup]");
	mouseModal 		= $('.mouse_modal');
	// tooltip 		= $(".tooltip");
	// scrollpane 		= $(".scroll-pane"),
	// flip			= $(".card");

if(equalheight.length || equalheight2.length){
  include("js/jquery.equalheights.js");
}
if(owl.length || owl2.length ){
	include("js/owl.carousel.js");
}
// if(popup.length){
// 	include("js/jquery.arcticmodal.js");
// }
if(mouseModal.length){
	include("js/jquery.arcticmodal.js");
}
// if(tooltip.length){
// 	include("js/tooltipster.bundle.min.js");
// }
// if(scrollpane.length){
// 	include("js/jquery.mousewheel.js");
// 	include("js/mwheelIntent.js");
// 	include("js/jquery.jscrollpane.min.js");
// }
// if(flip.length){
// 	include("js/jquery.flip.min.js");
// }


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}



var menu_selector = ".menu"; 

function onScroll(){
    var scroll_top = $(document).scrollTop();
    $(menu_selector + " a").each(function(){
        var hash = $(this).attr("href");
        var target = $(hash);
        if (target.position().top <= scroll_top && target.position().top + target.outerHeight() > scroll_top) {
            $(menu_selector + " a.current").removeClass("current");
            $(this).addClass("current");
        } else {
            $(this).removeClass("current");
        }
    });
}



$(document).ready(function(){

	$(".menu_btn, .menu_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })

    $(".menu_link").on('click ontouchstart', function(e){
  		var $this = $(this),
  			item  = $('.menu_link');

  			item.removeClass("current");
  			$this.toggleClass("current");
    })


    $(".menu").on("click","a", function (event) {
    	var body = $('body');
  		body.toggleClass('nav_overlay');

        event.preventDefault();
 
    });


    $(document).on("scroll", onScroll);
 
    $("a[href^=#data-]").click(function(e){
        e.preventDefault();
 
        $(document).off("scroll");
        $(menu_selector + " a.current").removeClass("current");
        $(this).addClass("current");
        var hash = $(this).attr("href");
        var target = $(hash);
 
        $("html, body").animate({
            scrollTop: target.offset().top
        }, 500, function(){
            window.location.hash = hash;
            $(document).on("scroll", onScroll);
        });
 
    });
 


	//------------  EQUALHEIGHT  ------------
	  	if ($(window).width() > 768){
	  		if(equalheight.length){
		  		$(equalheight).equalHeights();
		  	}
		}


	  	if(equalheight2.length){
	  		$(equalheight2).equalHeights();
	  	}


	//------------  CAROUSEL  ------------
		if(owl.length){

			owl.owlCarousel({
				items : 1,
			    singleItem : true,
			    pagination: true,
			    navigation: true
			});
				 
		}


	//------------  PopUp  ------------
		// if(popup.length){

		// 	popup.arcticmodal({
		// 		// closeOnOverlayClick: false
		// 	});
				 
		// }

		if ($(window).width() > 768){
			if( mouseModal.length ){

				mouseModal.on('click', function(e){
					e.preventDefault();

				 	var $this = $(this),
				 		$hiddenContent = $this.parent().parent().parent().find('.mouse_modal_hidden');

				 	if( $hiddenContent.length ) {
				 		$hiddenContent.arcticmodal({
				 			// 'close',
				 			closeOnOverlayClick: false
				 		});
				 	}


				});

			}

		}



	//------------  CAROUSEL  ------------
		// if(tooltip.length){

		// 	tooltip.tooltipster({
		// 		contentCloning: true	
		// 	});
				 
		// }


	//------------  SCROLL PANE  ------------
		// if(scrollpane.length){

	 //    	scrollpane.jScrollPane({
	 //    		verticalDragMaxHeight : 40
	 //    	});
	       
	 //    }


	//------------  FLIP  ------------
		// if(flip.length){

	 //    	flip.flip({
	 //    		trigger: 'hover'
	 //    	});
	       
	 //    }

	
/*--------------------  FORMS  ------------------*/

	function required(input){
		if (!input.val()){
			return false;
		}
		else{
			return true;
		}
	}

	function checkDigit(input){
		var digitPattern = /^\d+$/;
		if (!digitPattern.exec(input.val())){
			$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}
	}

	function checkEmail(input)
	{
	  var pattern=/^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,4}$/;
	  if(!pattern.exec($(input).val()))
		{
	    	return false;
		}
		else{
			return true;
		}
	}

	function checkRange(input){
		var current = $(input),
			minVal  = parseInt(current.data('min')),
			maxVal  = parseInt(current.data('max'));

		if (current.val() < minVal || current.val() > maxVal){
			$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}
	}

	function checkPhone(input) {
		var pattern=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
		if(!pattern.exec($(input).val()))
		{
	    	return false;
		}
		else{
			return true;
		}
	}

	function checkDate(input) {
		var pattern = /^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/;
		if(!pattern.exec($(input).val()))
		{
	    	$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}

	}


	$('.send_data').on('submit', function(event){
			event.preventDefault();

			var form = $(this),
				errors 	= 1,
				// name 	= form.find('[name="name"]'), 
				// phone 	= form.find('[name="phone"]'),
				email 		= form.find('[name="email"]'),
				description = form.find('[name="description"]');
				// subject = form.find('[name="subject"]');

			// if (!required(name)){
			// 	name.addClass('invalid');
			// 	errors = true;
			// }
			// else{
			// 	name.removeClass('invalid');
			// 	errors = false;
			// }

			// if (!required(phone) || !checkPhone(phone)){
			// 	phone.addClass('invalid');
			// 	errors = true;
			// }
			// else{
			// 	phone.removeClass('invalid');
			// 	errors = false;
			// }
			if (email.length){
				if (!required(email) || !checkEmail(email)){
					email.addClass('invalid');
					errors = true;
				}
				else{
					email.removeClass('invalid');
					errors = false;
				}	
			}
			if (description.length){
				if (!required(description)){
					description.addClass('invalid');
					errors = true;
				}
				else{
					description.removeClass('invalid');
					errors = false;
				}	
			}
		 	
			if(!errors){
		        var A = {
		          action : 'send',
		          // name : name.val(),
		          // phone : phone.val(),
		          email : email.val(),
		          description : description.val()
		          // subject : subject.val()
		        }

		        $.post('sendmail.php' , A , function(data){

		            form.find(".invalid").removeClass("invalid");
		            form.find("input").val('');
		            form.addClass('sent');		            

		            
		        });
		     }

		})

})